#-------------------------------------------------
#
# Project created by QtCreator 2018-07-22T20:12:50
#
#-------------------------------------------------

QT       += core gui sql
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ItemBuilder
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    tools/logger.cpp \
    tools/database.cpp \
    classes/meleeweapon.cpp \
    tools/store.cpp \
    windows/meleewindow.cpp \
    classes/meleematerial.cpp \
    classes/weaponenhancement.cpp \
    classes/weaponproperty.cpp \
    classes/weaponquality.cpp \
    cards/weaponcard.cpp \
    windows/cardeditor.cpp \
    windows/rangedwindow.cpp \
    classes/rangedmaterial.cpp \
    classes/rangedweapon.cpp

HEADERS += \
        mainwindow.h \
    tools/logger.h \
    tools/database.h \
    classes/meleeweapon.h \
    tools/store.h \
    windows/meleewindow.h \
    classes/meleematerial.h \
    classes/weaponenhancement.h \
    classes/weaponproperty.h \
    classes/weaponquality.h \
    cards/weaponcard.h \
    windows/cardeditor.h \
    windows/rangedwindow.h \
    classes/rangedmaterial.h \
    classes/rangedweapon.h

FORMS += \
        mainwindow.ui \
    windows/meleewindow.ui \
    windows/cardeditor.ui \
    windows/rangedwindow.ui \
