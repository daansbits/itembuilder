#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "ui_rangedwindow.h"
#include "windows/rangedwindow.h"
#include "windows/meleewindow.h"

#include <tools/database.h>
#include <tools/logger.h>

#include <QDebug>
#include <ui_meleewindow.h>



QPushButton* MainWindow::meleeWeaponButton;
QPushButton* MainWindow::rangedWeaponButton;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    Logger::truncateLog();
    Logger::writeMisc("Starting application.");
    Database::runChecks();

    Logger::writeInfo("Loading stuff from Database.");
    Database::fetchAllWeaponProperties();
    Database::fetchAllWeaponQualities();
    Database::fetchAllWeaponEnhancements();
    Database::fetchAllMeleeWeapons();
    Database::fetchAllMeleeMaterials();
    Database::fetchAllRangedWeapons();
    Database::fetchAllRangedMaterials();

    MainWindow::meleeWeaponButton = ui->but_melee;
    MainWindow::rangedWeaponButton = ui->but_ranged;
}

MainWindow::~MainWindow()
{
    Logger::writeMisc("Goodbye.\n");
    delete ui;
}

void MainWindow::on_but_exit_clicked()
{
    QApplication::quit();
}

void MainWindow::on_but_melee_clicked()
{
    MeleeWindow *mw = new MeleeWindow(this);
    ui->but_melee->setEnabled(false);
    mw->show();
}

void MainWindow::on_but_ranged_clicked()
{
    RangedWindow *rw = new RangedWindow(this);
    ui->but_ranged->setEnabled(false);
    rw->show();
}
