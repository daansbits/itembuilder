#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    static QPushButton *meleeWeaponButton;
    static QPushButton *rangedWeaponButton;
    ~MainWindow();

private slots:
    void on_but_exit_clicked();

    void on_but_melee_clicked();
    void on_but_ranged_clicked();

private:
    Ui::MainWindow *ui;

};

#endif // MAINWINDOW_H
