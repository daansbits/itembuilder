#ifndef LOGGER_H
#define LOGGER_H

#include <QDir>
#include <QString>



class Logger
{
public:
    Logger();
    static void writeError(const QString &message);
    static void writeWarning(const QString &message);
    static void writeDebug(const QString &message);
    static void writeInfo(const QString &message);
    static void writeMisc(const QString &message);
    static void truncateLog();
};

#endif // LOGGER_H
