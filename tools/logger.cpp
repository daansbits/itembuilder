#include "logger.h"

#include <QFile>
#include <QIODevice>
#include <QDateTime>
#include <QTextStream>

const QString LOGPATH = QDir::homePath() + "/journal.log";
const bool LOG_ERRORS = true;
const bool LOG_WARNINGS = true;
const bool LOG_INFO = true;
const bool LOG_DEBUG = true;

Logger::Logger()
{

}

void Logger::writeError(const QString &message) {
    QFile file(LOGPATH);
    if (!LOG_ERRORS) { return; }

    QString output = QDateTime::currentDateTime().toString("dd-MM-yy HH:mm:ss");
    output += " [EE] " + message;

    QTextStream out(&file);
    if (file.open(QIODevice::WriteOnly | QIODevice::Append)) {
        out << output << endl;
        out.flush();
    }
    file.close();
}


void Logger::writeWarning(const QString &message) {
    QFile file(LOGPATH);
    if (!LOG_WARNINGS) { return; }

    QString output = QDateTime::currentDateTime().toString("dd-MM-yy HH:mm:ss");
    output += " [WW] " + message;

    QTextStream out(&file);
    if (file.open(QIODevice::WriteOnly | QIODevice::Append)) {
        out << output << endl;
        out.flush();
    }
    file.close();
}

void Logger::writeDebug(const QString &message) {
    QFile file(LOGPATH);
    if (!LOG_DEBUG) { return; }

    QString output = QDateTime::currentDateTime().toString("dd-MM-yy HH:mm:ss");
    output += " [DD] " + message;

    QTextStream out(&file);
    if (file.open(QIODevice::WriteOnly | QIODevice::Append)) {
        out << output << endl;
        out.flush();
    }
    file.close();
}

void Logger::writeInfo(const QString &message) {
    QFile file(LOGPATH);
    if (!LOG_INFO) { return; }

    QString output = QDateTime::currentDateTime().toString("dd-MM-yy HH:mm:ss");
    output += " [II] " + message;

    QTextStream out(&file);
    if (file.open(QIODevice::WriteOnly | QIODevice::Append)) {
        out << output << endl;
        out.flush();
    }
    file.close();
}

void Logger::writeMisc(const QString &message) {
    QFile file(LOGPATH);

    QString output = QDateTime::currentDateTime().toString("dd-MM-yy HH:mm:ss");
    output += "      " + message;

    QTextStream out(&file);
    if (file.open(QIODevice::WriteOnly | QIODevice::Append)) {
        out << output << endl;
        out.flush();
    }
    file.close();
}

void Logger::truncateLog() {
    QFile file(LOGPATH);
    if (file.open(QIODevice::ReadWrite | QIODevice::Truncate))
        file.close();
}
