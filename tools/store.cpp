#include "logger.h"
#include "store.h"

QVector<MeleeWeapon> Store::meleeweapons;
QVector<MeleeMaterial> Store::meleematerials;
QVector<WeaponProperty> Store::weaponproperties;
QVector<WeaponEnhancement> Store::weaponenhancements;
QVector<WeaponQuality> Store::weaponqualities;
QVector<RangedWeapon> Store::rangedweapons;
QVector<RangedMaterial> Store::rangedmaterials;

QString Store::css = "<style>"
                     "center { margin-bottom: 10px; } "
                     "th { font-weight: 600; padding-right: 10px; text-align: left;} "
                     "td { padding-right: 30px; text-align: left;} "
                     "</style>";

Store::Store()
{

}
