#include "database.h"
#include "store.h"
#include "tools/logger.h"

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDebug>
#include <QSqlError>
#include <QSqlRecord>

#include <classes/meleeweapon.h>
#include <classes/meleematerial.h>
#include <classes/weaponproperty.h>
#include <classes/weaponenhancement.h>
#include <classes/rangedweapon.h>

const QString DB_PATH("/home/daan/Projects/Qt Creator/ItemBuilder/db_files/db.db");
const QString DB_DRIVER("QSQLITE");

static QSqlDatabase db;

Database::Database()
{
}

bool Database::runChecks() {
    Logger::writeInfo("Running database checks.");
    bool ok_driver;
    bool ok_path;
    bool ok_openable;
    bool ok_queryable;

    // Check if the driver is present
    ok_driver = QSqlDatabase::isDriverAvailable(DB_DRIVER);

    // Check if the database file exists
    QFileInfo file_info(DB_PATH);
    ok_path = file_info.exists();

    // Check if the database file is openable
    db = QSqlDatabase::addDatabase(DB_DRIVER);
    db.setDatabaseName(DB_PATH);
    ok_openable = ok_driver && db.open();

    // Check if the database file is queryable
    QSqlQuery query;
    ok_queryable = ok_openable && query.exec("SELECT * FROM AmmoType");

    // Close the database to clean up any loose ends
    QString lastError = db.lastError().text();
    if (!ok_queryable) { lastError = query.lastError().text(); }
    if (db.isOpen()) { db.close(); }

    // Log results to file
    ok_driver ? Logger::writeDebug("- SQLite driver found.") : Logger::writeWarning("- SQLite driver not found.");
    ok_path ? Logger::writeDebug("- Database found.") : Logger::writeWarning("- Database not found.");
    ok_openable ? Logger::writeDebug("- Database can be opened.") : Logger::writeWarning("- Database can not be opened.");
    ok_queryable ? Logger::writeDebug("- Database can be queried.") : Logger::writeWarning("- Database can not be queried.");

    bool success = ok_driver && ok_path && ok_openable && ok_queryable;
    if (success) { Logger::writeInfo("All checks passed."); }
    else { Logger::writeError("Failed to gain required access to the database: " + lastError); }

    return success;
}

void Database::fetchAllWeaponProperties()
{
    db.open();
    QSqlQuery query;
    if (!query.exec("SELECT * FROM WeaponProperty")) {
        Logger::writeError("Query failed while fetching weapon properties: " + query.lastError().text());
    }

    QSqlRecord rec = query.record();
    int ix_refId = rec.indexOf("refID");
    int ix_name = rec.indexOf("name");
    int ix_givesConstBonus = rec.indexOf("givesConstBonus");
    int ix_hasRange = rec.indexOf("hasRange");
    int ix_description = rec.indexOf("description");

    QVector<int> values;
    values << ix_refId << ix_name << ix_givesConstBonus << ix_hasRange << ix_description;
    int res = values.indexOf(-1);
    if (res != -1) { Logger::writeError("Unable to import column " + QString::number(res) + " while fetching weapon properties."); }

    while (query.next()) {
        WeaponProperty wp = WeaponProperty(
                    query.value(ix_refId).toString(),
                    query.value(ix_name).toString(),
                    query.value(ix_givesConstBonus).toBool(),
                    query.value(ix_hasRange).toBool(),
                    query.value(ix_description).toString(),
                    ""
                    );
        Store::weaponproperties.append(wp);
    }
    db.close();

    Logger::writeDebug("Loaded " + QString::number(Store::weaponproperties.length()) + " weapon properties into storage.");
}

void Database::fetchAllWeaponQualities()
{
    db.open();
    QSqlQuery query;
    if (!query.exec("SELECT * FROM WeaponQuality")) {
        Logger::writeError("Query failed while fetching weapon qualities: " + query.lastError().text());
    }

    QSqlRecord rec = query.record();
    int ix_refId = rec.indexOf("refID");
    int ix_name = rec.indexOf("name");
    int ix_priceMultiplier = rec.indexOf("priceMultiplier");
    int ix_isHiddenFromCard = rec.indexOf("hideFromCard");
    int ix_gainProperty = rec.indexOf("gainProperty");

    QVector<int> values;
    values << ix_refId << ix_name << ix_priceMultiplier << ix_gainProperty << ix_isHiddenFromCard;
    int res = values.indexOf(-1);
    if (res != -1) { Logger::writeError("Unable to import column " + QString::number(res) + " while fetching weapon qualities."); }

    while (query.next()) {
        QString propId = query.value(ix_gainProperty).toString();
        WeaponQuality wq;
        if (propId.length() != 0) {
            WeaponProperty *wp = Store::findInVector<WeaponProperty>(propId, &Store::weaponproperties);
            wq = WeaponQuality (
                        query.value(ix_refId).toString(),
                        query.value(ix_name).toString(),
                        query.value(ix_priceMultiplier).toFloat(),
                        query.value(ix_isHiddenFromCard).toBool(),
                        wp
                        );
        }
        else {
            wq = WeaponQuality (
                        query.value(ix_refId).toString(),
                        query.value(ix_name).toString(),
                        query.value(ix_priceMultiplier).toFloat(),
                        query.value(ix_isHiddenFromCard).toBool()
                        );
        }
        Store::weaponqualities.append(wq);
    }

    Logger::writeDebug("Loaded " + QString::number(Store::weaponqualities.length()) + " weapon qualities into storage.");
    db.close();
}

void Database::fetchAllWeaponEnhancements()
{
    db.open();
    QSqlQuery query;
    if (!query.exec("SELECT * FROM WeaponEnhancement")) {
        Logger::writeError("Query failed while fetching weapon enhancements: " + query.lastError().text());
    }

    QSqlRecord rec = query.record();
    int ix_refId = rec.indexOf("refID");
    int ix_name = rec.indexOf("name");
    int ix_priceMultiplier = rec.indexOf("priceMultiplier");
    int ix_priceModifier = rec.indexOf("priceModifier");
    int ix_gainProperty = rec.indexOf("gainProperty");

    QVector<int> values;
    values << ix_refId << ix_name << ix_priceMultiplier << ix_priceModifier << ix_gainProperty;
    int res = values.indexOf(-1);
    if (res != -1) { Logger::writeError("Unable to import column " + QString::number(res) + " while fetching weapon enhancements."); }

    while (query.next()) {
        QString propId = query.value(ix_gainProperty).toString();

        WeaponProperty *wp = Store::findInVector<WeaponProperty>(propId, &Store::weaponproperties);
        WeaponEnhancement we = WeaponEnhancement (
                    query.value(ix_refId).toString(),
                    query.value(ix_name).toString(),
                    query.value(ix_priceMultiplier).toFloat(),
                    query.value(ix_priceModifier).toInt(),
                    wp
                    );
        Store::weaponenhancements.append(we);
    }

    db.close();

    Logger::writeDebug("Loaded " + QString::number(Store::weaponenhancements.length()) + " weapon enhancements into storage.");
}

void Database::fetchAllMeleeWeapons() {
    db.open();
    QSqlQuery query;
    if (!query.exec("SELECT * FROM MeleeWeapon")) {
        Logger::writeError("Query failed while fetching melee weapons: " + query.lastError().text());
    }

    QSqlRecord rec = query.record();
    int ix_refId = rec.indexOf("refID");
    int ix_name = rec.indexOf("name");
    int ix_damage1h = rec.indexOf("damage1h");
    int ix_damage2h = rec.indexOf("damage2h");
    int ix_minReach = rec.indexOf("minReach");
    int ix_maxReach = rec.indexOf("maxReach");
    int ix_weight = rec.indexOf("weight");
    int ix_basePrice = rec.indexOf("basePrice");
    int ix_isModifiedByWood = rec.indexOf("isModifiedByWood");

    QVector<int> values;
    values << ix_refId << ix_name << ix_damage1h << ix_damage2h << ix_minReach << ix_maxReach << ix_weight << ix_basePrice << ix_isModifiedByWood;
    int res = values.indexOf(-1);
    if (res != -1) { Logger::writeError("Unable to import column " + QString::number(res) + " while fetching melee weapons."); }

    while (query.next()) {
        // See which properties this weapon has
        QSqlQuery propQuery;
        propQuery.prepare("SELECT wp.refID, mp.arg FROM Melee_Property AS mp INNER JOIN WeaponProperty AS wp ON mp.property = wp.refID WHERE mp.weapon = :weaponID");
        propQuery.bindValue(":weaponID", query.value(ix_refId).toString());
        if (!propQuery.exec()) {
            Logger::writeError("Query failed while fetching properties of " + query.value(ix_refId).toString() + ": " + query.lastError().text());
        }

        // Find each property to be applied in storage and add a pointer to it to a Vector
        QVector<std::pair<WeaponProperty*, QString>> wps;
        while (propQuery.next()) {
            QString propId = propQuery.value(0).toString();
            QString propArg = propQuery.value(1).toString();
            WeaponProperty *wp = Store::findInVector<WeaponProperty>(propId, &Store::weaponproperties);
            wps.append(std::pair<WeaponProperty*, QString>(wp, propArg));
        }

        MeleeWeapon mw = MeleeWeapon(
                    query.value(ix_refId).toString(),
                    query.value(ix_name).toString(),
                    query.value(ix_damage1h).toString(),
                    query.value(ix_damage2h).toString(),
                    query.value(ix_minReach).toInt(),
                    query.value(ix_maxReach).toInt(),
                    query.value(ix_weight).toInt(),
                    query.value(ix_basePrice).toFloat(),
                    query.value(ix_isModifiedByWood).toBool(),
                    wps
                    );
        Store::meleeweapons.append(mw);
    }

    Logger::writeDebug("Loaded " + QString::number(Store::meleeweapons.length()) + " melee weapons into storage.");
    db.close();
}

void Database::fetchAllMeleeMaterials()
{
    db.open();
    QSqlQuery query;
    if (!query.exec("SELECT * FROM MeleeMaterial ORDER BY damageModifier, priceMultiplier")) {
        Logger::writeError("Query failed while fetching melee materials: " + query.lastError().text());
    }

    QSqlRecord rec = query.record();
    int ix_refId = rec.indexOf("refID");
    int ix_name = rec.indexOf("name");
    int ix_damageMod = rec.indexOf("damageModifier");
    int ix_isMagic = rec.indexOf("isMagic");
    int ix_weightMod = rec.indexOf("weightModifier");
    int ix_enchant = rec.indexOf("enchant");
    int ix_doesHalfDamage = rec.indexOf("doesHalfDamage");
    int ix_priceMulti = rec.indexOf("priceMultiplier");

    QVector<int> values;
    values << ix_refId << ix_name << ix_damageMod << ix_isMagic << ix_weightMod << ix_enchant << ix_doesHalfDamage << ix_priceMulti;
    int res = values.indexOf(-1);
    if (res != -1) { Logger::writeError("Unable to import column " + QString::number(res) + " while fetching melee materials."); }

    while (query.next()) {
        MeleeMaterial mm = MeleeMaterial(
                    query.value(ix_refId).toString(),
                    query.value(ix_name).toString(),
                    query.value(ix_damageMod).toString(),
                    query.value(ix_isMagic).toBool(),
                    query.value(ix_weightMod).toInt(),
                    query.value(ix_enchant).toString(),
                    query.value(ix_doesHalfDamage).toBool(),
                    query.value(ix_priceMulti).toFloat()
                    );
        Store::meleematerials.append(mm);
    }
    db.close();

    Logger::writeDebug("Loaded " + QString::number(Store::meleematerials.length()) + " melee materials into storage.");
}

void Database::fetchAllRangedWeapons()
{
    db.open();
    QSqlQuery query;
    if (!query.exec("SELECT * FROM RangedWeapon")) {
        Logger::writeError("Query failed while fetching ranged weapons: " + query.lastError().text());
    }

    QSqlRecord rec = query.record();
    int ix_refId = rec.indexOf("refID");
    int ix_name = rec.indexOf("name");
    int ix_damage1h = rec.indexOf("damage1h");
    int ix_damage2h = rec.indexOf("damage2h");
    int ix_rangeClose = rec.indexOf("rangeClose");
    int ix_rangeMedium = rec.indexOf("rangeMedium");
    int ix_rangeFar = rec.indexOf("rangeFar");
    int ix_weight = rec.indexOf("weight");
    int ix_price = rec.indexOf("price");
    int ix_minReloadTime = rec.indexOf("minReloadTime");
    int ix_maxReloadTime = rec.indexOf("maxReloadTime");
    int ix_firesArrows = rec.indexOf("firesArrows");

    QVector<int> values;
    values << ix_refId << ix_name << ix_damage1h << ix_damage2h << ix_rangeClose << ix_rangeMedium << ix_rangeFar << ix_weight << ix_price << ix_minReloadTime << ix_maxReloadTime << ix_firesArrows;
    int res = values.indexOf(-1);
    if (res != -1) { Logger::writeError("Unable to import column " + QString::number(res) + " while fetching melee weapons."); }

    while (query.next()) {
        // See which properties this weapon has
        QSqlQuery propQuery;
        propQuery.prepare("SELECT wp.refID, rp.arg FROM Ranged_Property AS rp INNER JOIN RangedProperty AS rp ON rp.property = wp.refID WHERE rp.weapon = :weaponID");
        propQuery.bindValue(":weaponID", query.value(ix_refId).toString());
        if (!propQuery.exec()) {
            Logger::writeError("Query failed while fetching properties of " + query.value(ix_refId).toString() + ": " + query.lastError().text());
        }

        // Find each property to be applied in storage and add a pointer to it to a Vector
        QVector<std::pair<WeaponProperty*, QString>> wps;
        while (propQuery.next()) {
            QString propId = propQuery.value(0).toString();
            QString propArg = propQuery.value(1).toString();
            WeaponProperty *wp = Store::findInVector<WeaponProperty>(propId, &Store::weaponproperties);
            wps.append(std::pair<WeaponProperty*, QString>(wp, propArg));
        }

        RangedWeapon rw = RangedWeapon(
                    query.value(ix_refId).toString(),
                    query.value(ix_name).toString(),
                    query.value(ix_damage1h).toString(),
                    query.value(ix_damage2h).toString(),
                    query.value(ix_rangeClose).toInt(),
                    query.value(ix_rangeMedium).toInt(),
                    query.value(ix_rangeFar).toInt(),
                    query.value(ix_weight).toInt(),
                    query.value(ix_price).toInt(),
                    query.value(ix_minReloadTime).toInt(),
                    query.value(ix_maxReloadTime).toInt(),
                    query.value(ix_firesArrows).toBool(),
                    wps
                    );
        Store::rangedweapons.append(rw);
    }

    Logger::writeDebug("Loaded " + QString::number(Store::meleeweapons.length()) + " ranged weapons into storage.");
    db.close();
}

void Database::fetchAllRangedMaterials()
{
    db.open();
    QSqlQuery query;
    if (!query.exec("SELECT * FROM RangedMaterial ORDER BY rangeModifier, priceMultiplier")) {
        Logger::writeError("Query failed while fetching ranged materials: " + query.lastError().text());
    }

    QSqlRecord rec = query.record();
    int ix_refId = rec.indexOf("refID");
    int ix_name = rec.indexOf("name");
    int ix_rangeMod = rec.indexOf("rangeModifier");
    int ix_enchant = rec.indexOf("enchant");
    int ix_weightMod = rec.indexOf("weightModifier");
    int ix_priceMulti = rec.indexOf("priceMultiplier");

    QVector<int> values;
    values << ix_refId << ix_name << ix_rangeMod << ix_enchant << ix_weightMod << ix_priceMulti;
    int res = values.indexOf(-1);
    if (res != -1) { Logger::writeError("Unable to import column " + QString::number(res) + " while fetching melee materials."); }

    while (query.next()) {
        RangedMaterial rm = RangedMaterial(
                    query.value(ix_refId).toString(),
                    query.value(ix_name).toString(),
                    query.value(ix_rangeMod).toInt(),
                    query.value(ix_enchant).toInt(),
                    query.value(ix_weightMod).toInt(),
                    query.value(ix_priceMulti).toFloat()
                    );
        Store::rangedmaterials.append(rm);
    }
    db.close();

    Logger::writeDebug("Loaded " + QString::number(Store::meleematerials.length()) + " melee materials into storage.");
}
