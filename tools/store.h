#ifndef STORE_H
#define STORE_H

#include "logger.h"

#include <QVector>

#include <classes/meleeweapon.h>
#include <classes/weaponquality.h>
#include <classes/weaponenhancement.h>
#include <classes/weaponproperty.h>
#include <classes/rangedweapon.h>



class Store
{
public:
    Store();

    static QVector<WeaponProperty> weaponproperties;
    static QVector<WeaponQuality> weaponqualities;
    static QVector<WeaponEnhancement> weaponenhancements;

    static QVector<MeleeWeapon> meleeweapons;
    static QVector<MeleeMaterial> meleematerials;

    static QVector<RangedWeapon> rangedweapons;
    static QVector<RangedMaterial> rangedmaterials;

    static QString css;

    // TEMPLATE TO FIND SOMETHING IN ABOVE VECTORS
    template<class T>
    static T* findInVector(QString searchKey, QVector<T> *vector, bool findRefId = true) {
        T *res;
        if (findRefId) {
            res = std::find_if (vector->begin(), vector->end(), [&searchKey](const T &obj) {
                return obj.refID == searchKey;
            });
        }
        else {
            res = std::find_if (vector->begin(), vector->end(), [&searchKey](const T &obj) {
                return obj.name == searchKey;
            });
        }

        if (res == vector->end()) {
            Logger::writeWarning("Could not find property " + searchKey + ".");
        }

        return res;
    }

};

#endif // STORE_H
