#ifndef DATABASE_H
#define DATABASE_H

#include <QVector>

class Database
{
public:
    Database();
    static bool runChecks();
    static void connect();
    static void fetchAllWeaponProperties();
    static void fetchAllWeaponQualities();
    static void fetchAllWeaponEnhancements();

    static void fetchAllMeleeWeapons();
    static void fetchAllMeleeMaterials();

    static void fetchAllRangedWeapons();
    static void fetchAllRangedMaterials();
};

#endif // DATABASE_H
