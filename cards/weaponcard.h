#ifndef WEAPONCARD_H
#define WEAPONCARD_H

#include <classes/meleeweapon.h>
#include <classes/rangedweapon.h>
#include <classes/weaponquality.h>

enum WeaponType {
    UNSET, MELEE, RANGED
};

class WeaponCard
{
public:
    WeaponCard();
    WeaponCard(WeaponType weapontype);

    // Externals;
    MeleeWeapon *meleeweapon;
    RangedWeapon *rangedweapon;
    WeaponType type;

    // Result
    QString title;
    QString stats;
    QString propertiesSimple;
    QString propertiesExtensive;
    QString cardContents;

    void buildCard();
private:
    void buildTitle();
    void buildStats();
    void buildProperties();
};

#endif // WEAPONCARD_H
