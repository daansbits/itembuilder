#include "weaponcard.h"

#include <tools/store.h>
#include <QDebug>

WeaponCard::WeaponCard()
{
    this->type = WeaponType::UNSET;
}

WeaponCard::WeaponCard(WeaponType weapontype)
{
    this->type = weapontype;
}

void WeaponCard::buildCard() {
    if (this->type == WeaponType::UNSET) { return; }
    buildTitle();
    buildStats();
    buildProperties();

    this->cardContents = this->title + this->stats + this->propertiesExtensive;
}

// BUILDING THE CARD.
void WeaponCard::buildTitle() {
    if (this->type == WeaponType::MELEE) {
        bool isSecretlySilver = this->meleeweapon->ext_material->isSilvered && this->meleeweapon->ext_material == Store::findInVector<MeleeMaterial>("mmSteel", &Store::meleematerials);
        this->title = "<center><b>";

        // Add the quality if available
        if (this->meleeweapon->isQualitySet && !this->meleeweapon->ext_quality->isHiddenFromCard) { this->title += this->meleeweapon->ext_quality->name + " "; }

        // Add any properties
        if (this->meleeweapon->ext_material->isSilvered && !isSecretlySilver) { this->title += "Silvered "; }
        if (this->meleeweapon->ext_material->isRuned && !this->meleeweapon->ext_material->isMagic) { this->title += "Runed "; }

        // Add the material if available (Silver if it's silvered Steel)
        if (this->meleeweapon->isMaterialSet) {
            this->title += isSecretlySilver ? "Silver " : this->meleeweapon->ext_material->name + " ";
        }

        // Add the weapon name
        this->title += this->meleeweapon->name;
        this->title += "</b></center>\n\n";
    }
}

void WeaponCard::buildStats() {
    if (this->type == WeaponType::MELEE) {
        QString damageLabel = "Damage (" + this->meleeweapon->handed + ")";
        QString damageValue;

        if (this->meleeweapon->damage.length() > 0) {
            damageValue =  this->meleeweapon->damage + this->meleeweapon->damageModifier;
        }
        this->stats = "";
        this->stats += "<center>\n";
        this->stats += "<table>\n";
        this->stats += "    <tr>\n";
        this->stats += "        <th>" + damageLabel + "</th>\n";
        this->stats += "        <td>" + damageValue + "</td>\n";
        this->stats += "        <th>Encumbrance:</th>\n";
        this->stats += "        <td>" + QString::number(this->meleeweapon->weight) + "</td>\n";
        this->stats += "    </tr>\n";
        this->stats += "    <tr>\n";
        this->stats += "        <th>Reach:</th>\n";
        this->stats += "        <td>" + this->meleeweapon->reach + "</td>\n";
        this->stats += "        <th>Price:</th>\n";
        this->stats += "        <td>" + QString::number(this->meleeweapon->price) + "</td>\n";
        this->stats += "    </tr>\n";
        this->stats += "</table>\n";
        this->stats += "</center>\n";
    }
}

void WeaponCard::buildProperties() {
    this->propertiesSimple = "";
    this->propertiesExtensive = "";
    QString simpleProps = "<p><b>Weapon Attributes: </b> ";
    QString detailedProps = "\n";

    // TODO: Dehardcode this.
    WeaponProperty *magic = Store::findInVector<WeaponProperty>("opMagic", &Store::weaponproperties);
    WeaponProperty *silvered = Store::findInVector<WeaponProperty>("opMagic", &Store::weaponproperties);
    WeaponProperty *focus = Store::findInVector<WeaponProperty>("opFocus", &Store::weaponproperties);

    for (std::pair<WeaponProperty*, QString> prop : this->meleeweapon->db_properties) {
        simpleProps += prop.first->name;
        simpleProps += prop.second.length() > 0 ? " (" + prop.second + ")" : "";
        simpleProps += ", ";

        if (prop.second.length() > 0) {
            detailedProps += "<p>\n";
            detailedProps += "    <b>" + prop.first->name + " (" + prop.second + "):</b> ";
        }
        else {
            detailedProps += "<p>\n";
            detailedProps += "    <b>" + prop.first->name + ":</b> ";
        }

        detailedProps += prop.first->description + "\n";
        detailedProps += "</p>\n";

        detailedProps.replace("{{value}}", prop.second);
    }

    if (this->meleeweapon->ext_material->isMagic || this->meleeweapon->ext_material->isRuned || this->meleeweapon->ext_material->isSilvered) {
        simpleProps += magic->name + ", ";

        detailedProps += "<p>\n";
        detailedProps += "    <b>" + magic->name + ": </b>" + magic->description + "\n";
        detailedProps += "</p>";
    }

//    if (this->meleeweapon->ext_material->isSilvered && this->meleeweapon->) {
//        simpleProps += silvered->name + ", ";
//
//        detailedProps += "<p>";
//        detailedProps += "<span style=\"font-weight:600;\">" + silvered->name + ": </span>";
//        detailedProps += silvered->description;
//        detailedProps += "</p>";
//    }

    if (this->meleeweapon->ext_material->isFocus) {
        simpleProps += focus->name + ", ";

        detailedProps += "<p>\n";
        detailedProps += "    <b>" + focus->name + ": </b>" + focus->description + "\n";
        detailedProps += "</p>";
    }

    simpleProps = simpleProps.remove(simpleProps.lastIndexOf(","), 2);
    this->propertiesSimple += "</p>";
    this->propertiesSimple += simpleProps;

    this->propertiesExtensive = detailedProps;
}
