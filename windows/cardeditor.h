#ifndef CARDEDITOR_H
#define CARDEDITOR_H

#include <QDialog>

#include <cards/weaponcard.h>

namespace Ui {
class CardEditor;
}

class CardEditor : public QDialog
{
    Q_OBJECT

public:
    explicit CardEditor(QWidget *parent = 0);
    CardEditor(QWidget *parent, WeaponCard *wc);
    ~CardEditor();

private slots:
    void on_but_Underline_clicked();

    void on_but_Italic_clicked();

    void on_but_Bold_clicked();

    void on_but_AlignCentre_clicked();

    void on_but_Save_clicked();

    void on_but_Revert_clicked();

    void on_but_Cancel_clicked();

    void on_editor_textChanged();

    void on_but_Table_clicked();

private:
    Ui::CardEditor *ui;
    WeaponCard *card;
    QString css;
};

#endif // CARDEDITOR_H
