#include "rangedwindow.h"
#include "ui_rangedwindow.h"

#include <tools/database.h>
#include <tools/logger.h>
#include <tools/store.h>

#include <QDebug>
#include <mainwindow.h>

const int COL_ID = 0;
const int COL_NAME = 1;
const int COL_DAMAGE = 2;
const int COL_HANDED = 3;
const int COL_RANGE = 4;
const int COL_ENC = 5;
const int COL_PRICE = 6;
const int COL_QUAL = 7;

RangedWindow::RangedWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RangedWindow)
{
    ui->setupUi(this);

    this->cauldron = WeaponCard(WeaponType::UNSET);

    populateMainTable();
    populateMaterials();
    populateQualities();

//    on_cmb_weaponQuality_activated(ui->cmb_weaponQuality->currentText());
//    updateCauldron();
}

RangedWindow::~RangedWindow()
{
    delete ui;
}

void RangedWindow::closeEvent(QCloseEvent* /* event */)
{
    MainWindow::rangedWeaponButton->setEnabled(true);
}

void RangedWindow::populateMainTable()
{
    QVector<RangedWeapon> *mws = &Store::rangedweapons;
    ui->mainTable->setRowCount(mws->length());

    int rowNr = 0;
    QVector<RangedWeapon>::iterator item;
    for (item = mws->begin(); item != mws->end(); ++item) {
        ui->mainTable->setItem(rowNr, COL_ID, new QTableWidgetItem(item->refID));
        ui->mainTable->setItem(rowNr, COL_NAME, new QTableWidgetItem(item->name));
        ui->mainTable->setItem(rowNr, COL_DAMAGE, new QTableWidgetItem(item->damage));
        ui->mainTable->setItem(rowNr, COL_HANDED, new QTableWidgetItem(item->handed));
        ui->mainTable->setItem(rowNr, COL_RANGE, new QTableWidgetItem(item->range));
        ui->mainTable->setItem(rowNr, COL_ENC, new QTableWidgetItem(item->weight));
        ui->mainTable->setItem(rowNr, COL_PRICE, new QTableWidgetItem(item->price));
        ui->mainTable->setItem(rowNr, COL_QUAL, new QTableWidgetItem(item->properties));

        // Reinitialise integers into table, else they'll show up as blank.
        setFieldTo(item->refID, COL_RANGE, item->range);
        setFieldTo(item->refID, COL_PRICE, item->price);
        setFieldTo(item->refID, COL_ENC, item->weight);

        ++rowNr;
    }

    ui->mainTable->setSortingEnabled(true);
    ui->mainTable->setColumnHidden(0, true);
}

void RangedWindow::populateMaterials()
{
    QVector<RangedMaterial> *rms = &Store::rangedmaterials;
    QVector<RangedMaterial>::iterator item;

    for (item = rms->begin(); item != rms->end(); ++item) {
         ui->cmb_weaponMaterial->addItem(item->name);
    }
    ui->cmb_weaponMaterial->setCurrentIndex(-1);
}

void RangedWindow::populateQualities()
{
    QVector<WeaponQuality> *wqs = &Store::weaponqualities;
    QVector<WeaponQuality>::iterator item;

    int curIndex = -1;
    int finIndex = -1;
    float finZeroDiff = 1024;
    for (item = wqs->begin(); item != wqs->end(); ++item) {
        ui->cmb_weaponQuality->addItem(item->name);

        ++curIndex;
        float curZeroDiff = std::abs(item->priceMultiplier-1.0);
        if (curZeroDiff < finZeroDiff) {
            finZeroDiff = curZeroDiff;
            finIndex = curIndex;
        }
    }
    ui->cmb_weaponQuality->setCurrentIndex(finIndex);
}

void RangedWindow::updateCauldron()
{
    this->cauldron.buildCard();
    QString text = Store::css;
    text += this->cauldron.title;
    text += this->cauldron.stats;
    text += this->cauldron.propertiesSimple;
    ui->txt_cauldron->setHtml(text);
}

void RangedWindow::setToDefaultCardPreview()
{
    QString text;
    text += this->finalcard.title;
    text += this->finalcard.stats;
    text += this->finalcard.propertiesExtensive;
    this->finalcard.cardContents = text;

    ui->txt_card->setHtml(Store::css + text);
}

void RangedWindow::setToChangedCardPreview()
{
    ui->txt_card->setHtml(Store::css + this->finalcard.cardContents);
}

bool RangedWindow::setFieldTo(QString refID, int column, QString text) {
    int rowCount = ui->mainTable->rowCount();
    for (int i=0; i<rowCount; ++i) {
        if (ui->mainTable->item(i, 0)->text() == refID) {
            ui->mainTable->item(i, column)->setText(text);
            return true;
        }
    }
    return false;
}

bool RangedWindow::setFieldTo(QString refID, int column, int value) {
    int rowCount = ui->mainTable->rowCount();
    for (int i=0; i<rowCount; ++i) {
        if (ui->mainTable->item(i, 0)->text() == refID) {
            ui->mainTable->item(i, column)->setData(Qt::EditRole, value);
            return true;
        }
    }
    return false;
}

void RangedWindow::on_cmb_weaponMaterial_activated(const QString &arg1)
{
    RangedMaterial *rm = Store::findInVector<RangedMaterial>(arg1, &Store::rangedmaterials, false);

    for (RangedWeapon &weapon : Store::rangedweapons) {
        weapon.ext_material = rm;
        weapon.isMaterialSet = true;
        weapon.recalculatePrice();
        weapon.recalculateWeight();
        setFieldTo(weapon.refID, COL_PRICE, weapon.price);
        setFieldTo(weapon.refID, COL_ENC, weapon.weight);
    }

    updateCauldron();
}

void RangedWindow::on_cmb_weaponQuality_activated(const QString &arg1)
{
    WeaponQuality *wq = Store::findInVector<WeaponQuality>(arg1, &Store::weaponqualities, false);

    for (RangedWeapon &weapon : Store::rangedweapons) {
        weapon.isQualitySet = true;
        weapon.ext_quality = wq;
        weapon.recalculatePrice();
        setFieldTo(weapon.refID, COL_PRICE, weapon.price);
    }

    updateCauldron();
}

void RangedWindow::on_mainTable_cellClicked(int row, int /* column */)
{
    QString clickedId = ui->mainTable->item(row, COL_ID)->text();
    this->cauldron.rangedweapon = Store::findInVector<RangedWeapon>(clickedId, &Store::rangedweapons);
    this->cauldron.type = WeaponType::RANGED;

    updateCauldron();
}

void RangedWindow::on_chk_Focus_clicked(bool checked)
{
    for (MeleeMaterial &material : Store::meleematerials) {
        material.isFocus = checked;
    }

    updateCauldron();
}

void RangedWindow::on_but_moveToCard_clicked()
{
    this->finalcard = this->cauldron;
    this->setToDefaultCardPreview();
}

void RangedWindow::on_but_revertCard_clicked()
{
    this->setToDefaultCardPreview();
}

void RangedWindow::on_but_editCard_clicked()
{
    this->ce = new CardEditor(this, &this->finalcard);
    if (this->ce->exec() == QDialog::Accepted) {
        this->setToChangedCardPreview();
    }
}
