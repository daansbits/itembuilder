#ifndef RANGEDWINDOW_H
#define RANGEDWINDOW_H

#include "cardeditor.h"

#include <QDialog>

#include <cards/weaponcard.h>

namespace Ui {
class RangedWindow;
}

class RangedWindow : public QDialog
{
    Q_OBJECT

public:
    explicit RangedWindow(QWidget *parent = 0);
    ~RangedWindow();

private slots:
    void on_cmb_weaponMaterial_activated(const QString &arg1);
    void on_cmb_weaponQuality_activated(const QString &arg1);

    void on_mainTable_cellClicked(int row, int column);

    void on_chk_Focus_clicked(bool checked);

    void on_but_moveToCard_clicked();

    void on_but_revertCard_clicked();

    void on_but_editCard_clicked();

private:
    Ui::RangedWindow *ui;
    CardEditor *ce;

    WeaponCard cauldron;
    WeaponCard finalcard;

    void populateMainTable();
    void populateMaterials();
    void populateQualities();
    void updateCauldron();
    void setToDefaultCardPreview();
    void setToChangedCardPreview();
    bool setFieldTo(QString refID, int column, QString text);
    bool setFieldTo(QString refID, int column, int value);
    void closeEvent(QCloseEvent *event);
};

#endif // RANGEDWINDOW_H
