#include "meleewindow.h"
#include "ui_meleewindow.h"

#include <tools/database.h>
#include <tools/logger.h>
#include <tools/store.h>

#include <QDebug>
#include <mainwindow.h>

const int COL_ID = 0;
const int COL_NAME = 1;
const int COL_DAMAGE = 2;
const int COL_HANDED = 3;
const int COL_REACH = 4;
const int COL_ENC = 5;
const int COL_PRICE = 6;
const int COL_QUAL = 7;

MeleeWindow::MeleeWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MeleeWindow)
{
    ui->setupUi(this);

    this->cauldron = WeaponCard(WeaponType::UNSET);

    populateMainTable();
    populateMaterials();
    populateQualities();

    on_cmb_quality_activated(ui->cmb_quality->currentText());
    updateCauldron();
}

MeleeWindow::~MeleeWindow()
{
    delete ui;
}

void MeleeWindow::closeEvent(QCloseEvent* /* event */)
{
    MainWindow::meleeWeaponButton->setEnabled(true);
}

void MeleeWindow::populateMainTable()
{
    QVector<MeleeWeapon> *mws = &Store::meleeweapons;
    ui->mainTable->setRowCount(mws->length());

    int rowNr = 0;
    QVector<MeleeWeapon>::iterator item;
    for (item = mws->begin(); item != mws->end(); ++item) {
        ui->mainTable->setItem(rowNr, COL_ID, new QTableWidgetItem(item->refID));
        ui->mainTable->setItem(rowNr, COL_NAME, new QTableWidgetItem(item->name));
        ui->mainTable->setItem(rowNr, COL_DAMAGE, new QTableWidgetItem(item->damage));
        ui->mainTable->setItem(rowNr, COL_HANDED, new QTableWidgetItem(item->handed));
        ui->mainTable->setItem(rowNr, COL_REACH, new QTableWidgetItem(item->reach));
        ui->mainTable->setItem(rowNr, COL_ENC, new QTableWidgetItem(item->weight));
        ui->mainTable->setItem(rowNr, COL_PRICE, new QTableWidgetItem(item->price));
        ui->mainTable->setItem(rowNr, COL_QUAL, new QTableWidgetItem(item->properties));

        // Reinitialise integers into table, else they'll show up as blank.
        setFieldTo(item->refID, COL_PRICE, item->price);
        setFieldTo(item->refID, COL_ENC, item->weight);

        ++rowNr;
    }

    ui->mainTable->setSortingEnabled(true);
    ui->mainTable->setColumnHidden(0, true);
}

void MeleeWindow::populateMaterials()
{
    QVector<MeleeMaterial> *mms = &Store::meleematerials;
    QVector<MeleeMaterial>::iterator item;

    for (item = mms->begin(); item != mms->end(); ++item) {
         ui->cmb_material->addItem(item->name);
    }
    ui->cmb_material->setCurrentIndex(-1);
}

void MeleeWindow::populateQualities()
{
    QVector<WeaponQuality> *wqs = &Store::weaponqualities;
    QVector<WeaponQuality>::iterator item;

    int curIndex = -1;
    int finIndex = -1;
    float finZeroDiff = 1024;
    for (item = wqs->begin(); item != wqs->end(); ++item) {
        ui->cmb_quality->addItem(item->name);

        ++curIndex;
        float curZeroDiff = std::abs(item->priceMultiplier-1.0);
        if (curZeroDiff < finZeroDiff) {
            finZeroDiff = curZeroDiff;
            finIndex = curIndex;
        }
    }
    ui->cmb_quality->setCurrentIndex(finIndex);
}

void MeleeWindow::updateCauldron()
{
    this->cauldron.buildCard();
    QString text = Store::css;
    text += this->cauldron.title;
    text += this->cauldron.stats;
    text += this->cauldron.propertiesSimple;
    ui->txt_cauldron->setHtml(text);
}

void MeleeWindow::setToDefaultCardPreview()
{
    QString text;
    text += this->finalcard.title;
    text += this->finalcard.stats;
    text += this->finalcard.propertiesExtensive;
    this->finalcard.cardContents = text;

    ui->txt_card->setHtml(Store::css + text);
}

void MeleeWindow::setToChangedCardPreview()
{
    ui->txt_card->setHtml(Store::css + this->finalcard.cardContents);
}

bool MeleeWindow::setFieldTo(QString refID, int column, QString text) {
    int rowCount = ui->mainTable->rowCount();
    for (int i=0; i<rowCount; ++i) {
        if (ui->mainTable->item(i, 0)->text() == refID) {
            ui->mainTable->item(i, column)->setText(text);
            return true;
        }
    }
    return false;
}

bool MeleeWindow::setFieldTo(QString refID, int column, int value) {
    int rowCount = ui->mainTable->rowCount();
    for (int i=0; i<rowCount; ++i) {
        if (ui->mainTable->item(i, 0)->text() == refID) {
            ui->mainTable->item(i, column)->setData(Qt::EditRole, value);
            return true;
        }
    }
    return false;
}

void MeleeWindow::on_cmb_material_activated(const QString &arg1)
{
    MeleeMaterial *mm = Store::findInVector<MeleeMaterial>(arg1, &Store::meleematerials, false);

    for (MeleeWeapon &weapon : Store::meleeweapons) {
        weapon.ext_material = mm;
        weapon.isMaterialSet = true;
        weapon.recalculatePrice();
        weapon.recalculateWeight();
        weapon.recalculateDamage();
        setFieldTo(weapon.refID, COL_PRICE, weapon.price);
        setFieldTo(weapon.refID, COL_ENC, weapon.weight);

        ui->chk_Runed->setEnabled(!mm->isMagic);
    }

    updateCauldron();
}

void MeleeWindow::on_cmb_quality_activated(const QString &arg1)
{
    WeaponQuality *wq = Store::findInVector<WeaponQuality>(arg1, &Store::weaponqualities, false);

    for (MeleeWeapon &weapon : Store::meleeweapons) {
        weapon.isQualitySet = true;
        weapon.ext_quality = wq;
        weapon.recalculatePrice();
        setFieldTo(weapon.refID, COL_PRICE, weapon.price);
    }

    updateCauldron();
}

void MeleeWindow::on_mainTable_cellClicked(int row, int /* column */)
{
    QString clickedId = ui->mainTable->item(row, COL_ID)->text();
    this->cauldron.meleeweapon = Store::findInVector<MeleeWeapon>(clickedId, &Store::meleeweapons);
    this->cauldron.type = WeaponType::MELEE;

    updateCauldron();
}

void MeleeWindow::on_chk_Silver_clicked(bool checked)
{
    for (MeleeMaterial &material : Store::meleematerials) {
        material.isSilvered = checked;
    }

    updateCauldron();
}

void MeleeWindow::on_chk_Runed_clicked(bool checked)
{
    for (MeleeMaterial &material : Store::meleematerials) {
        material.isRuned = checked;
    }

    updateCauldron();
}

void MeleeWindow::on_chk_Focus_clicked(bool checked)
{
    for (MeleeMaterial &material : Store::meleematerials) {
        material.isFocus = checked;
    }

    updateCauldron();
}

void MeleeWindow::on_but_moveToCard_clicked()
{
    this->finalcard = this->cauldron;
    this->setToDefaultCardPreview();
}

void MeleeWindow::on_but_revertCard_clicked()
{
    this->setToDefaultCardPreview();
}

void MeleeWindow::on_but_editCard_clicked()
{
    this->ce = new CardEditor(this, &this->finalcard);
    if (this->ce->exec() == QDialog::Accepted) {
        this->setToChangedCardPreview();
    }
}
