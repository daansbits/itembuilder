#ifndef MELEEWINDOW_H
#define MELEEWINDOW_H

#include "cardeditor.h"

#include <QDialog>

#include <cards/weaponcard.h>

namespace Ui {
class MeleeWindow;
}

class MeleeWindow : public QDialog
{
    Q_OBJECT

public:
    explicit MeleeWindow(QWidget *parent = 0);
    ~MeleeWindow();

private slots:
    void on_cmb_material_activated(const QString &arg1);
    void on_cmb_quality_activated(const QString &arg1);

    void on_mainTable_cellClicked(int row, int column);

    void on_chk_Silver_clicked(bool checked);

    void on_chk_Runed_clicked(bool checked);

    void on_chk_Focus_clicked(bool checked);

    void on_but_moveToCard_clicked();

    void on_but_revertCard_clicked();

    void on_but_editCard_clicked();

private:
    Ui::MeleeWindow *ui;
    CardEditor *ce;

    WeaponCard cauldron;
    WeaponCard finalcard;

    void populateMainTable();
    void populateMaterials();
    void populateQualities();
    void updateCauldron();
    void setToDefaultCardPreview();
    void setToChangedCardPreview();
    bool setFieldTo(QString refID, int column, QString text);
    bool setFieldTo(QString refID, int column, int value);
    void closeEvent(QCloseEvent *event);
};

#endif // MELEEWINDOW_H
