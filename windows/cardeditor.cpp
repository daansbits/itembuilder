#include "cardeditor.h"
#include "ui_cardeditor.h"

#include <QDebug>

#include <tools/store.h>


CardEditor::CardEditor(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CardEditor)
{
    ui->setupUi(this);
}

CardEditor::CardEditor(QWidget *parent, WeaponCard *wc) :
    QDialog(parent),
    ui(new Ui::CardEditor)
{
    ui->setupUi(this);
    this->card = wc;
    if (this->card->cardContents.length() > 0) {
        ui->editor->setPlainText(this->card->cardContents);
    }
}

CardEditor::~CardEditor()
{
    delete ui;
}

void CardEditor::on_but_Underline_clicked()
{
    ui->editor->insertPlainText("<u></u>");
}

void CardEditor::on_but_Italic_clicked()
{
    ui->editor->insertPlainText("<i></i>");
}

void CardEditor::on_but_Bold_clicked()
{
    ui->editor->insertPlainText("<b></b>");
}

void CardEditor::on_but_AlignCentre_clicked()
{
    ui->editor->insertPlainText("<center></center>");
}

void CardEditor::on_but_Save_clicked()
{
    this->card->cardContents = ui->editor->toPlainText();
    this->accept();
}

void CardEditor::on_but_Revert_clicked()
{
    ui->editor->setPlainText(this->card->cardContents);
}

void CardEditor::on_but_Cancel_clicked()
{
    this->reject();
}

void CardEditor::on_editor_textChanged()
{
    QString slide = Store::css;
    slide += ui->editor->toPlainText();
    ui->viewer->setHtml(slide);
}

void CardEditor::on_but_Table_clicked()
{
    ui->editor->insertPlainText("<table>\n");
    ui->editor->insertPlainText("<tr>\n");
    ui->editor->insertPlainText("    <td></td>\n");
    ui->editor->insertPlainText("    <td></td>\n");
    ui->editor->insertPlainText("</tr>\n");
    ui->editor->insertPlainText("<tr>\n");
    ui->editor->insertPlainText("    <td></td>\n");
    ui->editor->insertPlainText("    <td></td>\n");
    ui->editor->insertPlainText("</tr>\n");
    ui->editor->insertPlainText("</table>\n");
}
