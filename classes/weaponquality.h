#ifndef WEAPONQUALITY_H
#define WEAPONQUALITY_H

#include "weaponproperty.h"



class WeaponQuality
{
public:
    WeaponQuality();
    WeaponQuality(QString refID, QString name, float priceMultiplier, bool isHiddenFromCard);
    WeaponQuality(QString refID, QString name, float priceMultiplier, bool isHiddenFromCard, WeaponProperty *gained);

    QString refID;
    QString name;
    float priceMultiplier;
    WeaponProperty *gained;
    bool isHiddenFromCard;
};

#endif // WEAPONQUALITY_H
