#ifndef RANGEDMATERIAL_H
#define RANGEDMATERIAL_H

#include <QString>



class RangedMaterial
{
public:
    RangedMaterial();
    RangedMaterial(QString refID, QString name, int rangeModifier, int enchant, int weightModifier, float priceMultiplier);

    QString refID;
    QString name;
    int rangeModifier;
    int weightModifier;
    int enchant;
    float priceMultiplier;

    // UI switches
    bool isFocus;
};

#endif // RANGEDMATERIAL_H
