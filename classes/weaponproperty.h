#ifndef WEAPONPROPERTY_H
#define WEAPONPROPERTY_H

#include <QString>



class WeaponProperty
{
public:
    WeaponProperty();
    WeaponProperty(QString refID, QString name, bool givesConstBonus, bool hasRange, QString description, QString arg);

    QString refID;
    QString name;
    bool givesConstBonus;
    bool hasRange;
    QString description;
};

#endif // WEAPONPROPERTY_H
