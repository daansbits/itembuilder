#ifndef RANGEDWEAPON_H
#define RANGEDWEAPON_H

#include "rangedmaterial.h"
#include "weaponproperty.h"
#include "weaponquality.h"

#include <QString>
#include <QVector>



class RangedWeapon
{
public:
    RangedWeapon();
    RangedWeapon(
            QString db_refID, QString db_name, QString db_damage1h, QString db_damage2h,
            int db_rangeClose, int db_rangeMedium, int db_rangeFar, int db_weight, int db_price,
            int db_minReloadTime, int db_maxReloadTime, bool db_firesArrows, QVector<std::pair<WeaponProperty*, QString>> db_properties
            );

    // For table;
    QString refID;
    QString name;
    QString damage;
    QString handed;
    QString range;
    int weight;
    int price;
    QString properties;

    // Native properties from the weapon
    QVector<std::pair <WeaponProperty*, QString>> db_properties;

    // Externals
    RangedMaterial *ext_material;
    WeaponQuality *ext_quality;
    QVector<std::pair<WeaponProperty*, QString>> ext_properties;
    bool isFocus;

    // Checks
    bool isMaterialSet;
    bool isQualitySet;

    void recalculatePrice();
    void recalculateWeight();
private:
    // Raw data from db
    QString db_refID;
    QString db_name;
    QString db_damage1h;
    QString db_damage2h;
    int db_rangeClose;
    int db_rangeMedium;
    int db_rangeFar;
    int db_weight;
    int db_price;
    int db_minReloadTime;
    int db_maxReloadTime;
    bool db_firesArrows; // rather than bolts

    void prepare();
};

#endif // RANGEDWEAPON_H
