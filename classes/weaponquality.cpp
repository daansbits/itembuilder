#include "weaponquality.h"

WeaponQuality::WeaponQuality()
{

}

WeaponQuality::WeaponQuality(QString refID, QString name, float priceMultiplier, bool isHiddenFromCard)
{
    this->refID = refID;
    this->name = name;
    this->priceMultiplier = priceMultiplier;
    this->isHiddenFromCard = isHiddenFromCard;
}

WeaponQuality::WeaponQuality(QString refID, QString name, float priceMultiplier, bool isHiddenFromCard, WeaponProperty *gained)
{
    this->refID = refID;
    this->name = name;
    this->priceMultiplier = priceMultiplier;
    this->isHiddenFromCard = isHiddenFromCard;
    this->gained = gained;
}
