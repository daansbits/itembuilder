#ifndef MELEEMATERIALS_H
#define MELEEMATERIALS_H

#include <QString>



class MeleeMaterial
{
public:
    MeleeMaterial();
    MeleeMaterial(QString refID, QString name, QString damageModifier, bool isMagic, int weightModifier, QString enchant, bool doesHalfDamage, float priceMultiplier);

    QString refID;
    QString name;
    QString damageModifier;
    bool isMagic;
    int weightModifier;
    QString enchant;
    bool doesHalfDamage;
    float priceMultiplier;

    // UI switches
    bool isSilvered;
    bool isRuned;
    bool isFocus;
};

#endif // MELEEMATERIALS_H
