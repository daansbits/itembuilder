#include "rangedweapon.h"

#include <tools/store.h>

RangedWeapon::RangedWeapon()
{

}

RangedWeapon::RangedWeapon(QString db_refID, QString db_name, QString db_damage1h, QString db_damage2h,
                           int db_rangeClose, int db_rangeMedium, int db_rangeFar, int db_weight,
                           int db_price, int db_minReloadTime, int db_maxReloadTime,
                           bool db_firesArrows, QVector<std::pair<WeaponProperty*, QString>> db_properties)
{
    this->db_refID = db_refID;
    this->db_name = db_name;
    this->db_damage1h = db_damage1h;
    this->db_damage2h = db_damage2h;
    this->db_rangeClose = db_rangeClose;
    this->db_rangeMedium = db_rangeMedium;
    this->db_rangeFar = db_rangeFar;
    this->db_weight = db_weight;
    this->db_price = db_price;
    this->db_minReloadTime = db_minReloadTime;
    this->db_maxReloadTime = db_maxReloadTime;
    this->db_firesArrows = db_firesArrows;
    this->db_properties = db_properties;

    this->prepare();
}

void RangedWeapon::prepare() {
    this->refID = this->db_refID;
    this->name = this->db_name;

    if (this->db_damage1h.length() > 0 && this->db_damage2h > 0) {
        this->damage = db_damage1h + " (" + db_damage2h + ")";
        this->handed = "1.5H";
    }
    else {
        this->damage = this->db_damage1h.length() > 0 ? this->db_damage1h : this->db_damage2h;
        this->handed = this->db_damage1h.length() > 0 ? "1H" : "2H";
    }

    this->range = QString::number(this->db_rangeClose) + "/" + QString::number(this->db_rangeMedium) + "/" + QString::number(this->db_rangeFar);
    this->weight = this->db_weight;
    this->price = this->db_price;

    for (int i=0; i < this->db_properties.length(); ++i) {
        QString propName = this->db_properties.value(i).first->name;
        QString propValue = this->db_properties.value(i).second;
        this->properties += this->db_properties.value(i).first->name;
        if (propValue.length() > 0) { this->properties += " (" + propValue + ")"; }
        if (i != this->db_properties.length()-1) { this->properties += ", "; }
    }
}

void RangedWeapon::recalculatePrice() {
    WeaponEnhancement *focus = Store::findInVector("reFocus", &Store::weaponenhancements);
    this->price = this->db_price * this->ext_material->priceMultiplier;

    if (this->isFocus) {
        this->price = this->price * focus->priceMultiplier + focus->priceModifier;
    }
}

void RangedWeapon::recalculateWeight() {
    this->weight = this->db_weight + this->ext_material->weightModifier;
}


