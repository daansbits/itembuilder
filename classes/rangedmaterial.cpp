#include "rangedmaterial.h"

RangedMaterial::RangedMaterial()
{

}

RangedMaterial::RangedMaterial(QString refID, QString name, int rangeModifier, int enchant, int weightModifier, float priceMultiplier)
{
    this->refID = refID;
    this->name = name;
    this->rangeModifier = rangeModifier;
    this->enchant = enchant;
    this->weightModifier = weightModifier;
    this->priceMultiplier = priceMultiplier;
}
