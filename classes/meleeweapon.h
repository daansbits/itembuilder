#ifndef MELEEWEAPON_H
#define MELEEWEAPON_H

#include "meleematerial.h"
#include "weaponenhancement.h"
#include "weaponproperty.h"
#include "weaponquality.h"

#include <QString>
#include <QVector>



class MeleeWeapon
{
public:
    MeleeWeapon();
    MeleeWeapon(
            QString db_refID, QString db_name, QString db_damage1h, QString db_damage2h,
            int db_minReach, int db_maxReach, int db_weight, int db_basePrice, bool db_wood,
            QVector<std::pair<WeaponProperty*, QString>> properties
            );

    // For table
    QString refID;
    QString name;
    QString damage;
    QString damageModifier;
    QString handed;
    QString reach;
    int weight;
    int price;
    QString properties;

    // Native properties from the weapon
    QVector<std::pair<WeaponProperty*, QString>> db_properties;

    // Externals
    MeleeMaterial *ext_material;
    WeaponQuality *ext_quality;
    QVector<std::pair<WeaponProperty*, QString>> ext_properties;
    QVector<WeaponEnhancement*> ext_enhancements;

    // Check if certain options are set
    bool isMaterialSet;
    bool isQualitySet;

    void recalculatePrice();
    void recalculateWeight();
    void recalculateDamage();
private:
    // Raw data from db
    QString db_refID;
    QString db_name;
    QString db_damage1h;
    QString db_damage2h;
    int db_minReach;
    int db_maxReach;
    int db_weight;
    int db_basePrice;
    bool db_isModifiedByWood;

    void prepare();
};

#endif // MELEEWEAPON_H
