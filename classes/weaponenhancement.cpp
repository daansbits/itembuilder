#include "weaponenhancement.h"

WeaponEnhancement::WeaponEnhancement()
{

}

WeaponEnhancement::WeaponEnhancement(QString refID, QString name, float priceMultiplier, int priceModifier, WeaponProperty *gained)
{
    this->refID = refID;
    this->name = name;
    this->priceMultiplier = priceMultiplier;
    this->priceModifier = priceModifier;
    this->gained = gained;
}
