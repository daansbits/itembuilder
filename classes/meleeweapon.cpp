#include "meleeweapon.h"

#include <QDebug>

MeleeWeapon::MeleeWeapon()
{

}

MeleeWeapon::MeleeWeapon(QString db_refID, QString db_name, QString db_damage1h, QString db_damage2h,
                         int db_minReach, int db_maxReach, int db_weight, int db_basePrice, bool db_wood,
                         QVector<std::pair<WeaponProperty*, QString>> properties)
{
    this->db_refID = db_refID;
    this->db_name = db_name;
    this->db_damage1h = db_damage1h;
    this->db_damage2h = db_damage2h;
    this->db_minReach = db_minReach;
    this->db_maxReach = db_maxReach;
    this->db_weight = db_weight;
    this->db_basePrice = db_basePrice;
    this->db_isModifiedByWood = db_wood;
    this->db_properties = properties;

    this->prepare();
}

void MeleeWeapon::prepare() {
    this->refID = this->db_refID;
    this->name = this->db_name;    
    this->weight = this->db_weight;

    if (this->db_damage1h.length() > 0 && this->db_damage2h > 0) {
        this->damage = db_damage1h + " (" + db_damage2h + ")";
        this->handed = "1.5H";
    }
    else {
        this->damage = this->db_damage1h.length() > 0 ? this->db_damage1h : this->db_damage2h;
        this->handed = this->db_damage1h.length() > 0 ? "1H" : "2H";
    }

    if (this->db_minReach == this->db_maxReach || this->db_minReach == 1) { this->reach = QString::number(this->db_maxReach) + "m"; }
    else { this->reach = QString::number(this->db_minReach) + "-" + QString::number(this->db_maxReach) + "m"; }

    this->price = this->db_basePrice;

    for (int i=0; i < this->db_properties.length(); ++i) {
        QString propName = this->db_properties.value(i).first->name;
        QString propValue = this->db_properties.value(i).second;
        this->properties += this->db_properties.value(i).first->name;
        if (propValue.length() > 0) { this->properties += " (" + propValue + ")"; }
        if (i != this->db_properties.length()-1) { this->properties += ", "; }
    }
}

void MeleeWeapon::recalculatePrice() {
    float qualityMulti = this->isQualitySet ? this->ext_quality->priceMultiplier : 1.0;
    float materialMulti = this->isMaterialSet ? this->ext_material->priceMultiplier : 1.0;

    int price = this->db_basePrice * qualityMulti * materialMulti;
    this->price = price;
}

void MeleeWeapon::recalculateWeight() {
    int weight = this->db_weight + this->ext_material->weightModifier;
    this->weight = weight;
}

void MeleeWeapon::recalculateDamage() {
    if (this->db_damage1h.length() > 0 && this->db_damage2h > 0) {
        this->damage = db_damage1h + " (" + db_damage2h + ")";
        this->handed = "1.5H";
    }
    else {
        this->damage = this->db_damage1h.length() > 0 ? this->db_damage1h : this->db_damage2h;
        this->handed = this->db_damage1h.length() > 0 ? "1H" : "2H";
    }

    this->damageModifier = "";
    if (this->ext_material->damageModifier != "0" && this->damage.length() > 0) {
        this->damageModifier += " +" + this->ext_material->damageModifier;
    }

    if (this->ext_material->doesHalfDamage && this->damage.length() > 0) {
        this->damageModifier += " /2";
    }
}

