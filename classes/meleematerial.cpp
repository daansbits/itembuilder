#include "meleematerial.h"

MeleeMaterial::MeleeMaterial()
{

}

MeleeMaterial::MeleeMaterial(QString refID, QString name, QString damageModifier, bool isMagic, int weightModifier, QString enchant, bool doesHalfDamage, float priceMultiplier)
{
    this->refID = refID;
    this->name = name;
    this->damageModifier = damageModifier;
    this->isMagic = isMagic;
    this->weightModifier = weightModifier;
    this->enchant = enchant;
    this->doesHalfDamage = doesHalfDamage;
    this->priceMultiplier = priceMultiplier;

    this->isSilvered = false;
    this->isFocus = false;
    this->isRuned = false;
}

