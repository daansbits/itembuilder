#include "weaponproperty.h"

WeaponProperty::WeaponProperty()
{

}

WeaponProperty::WeaponProperty(QString refID, QString name, bool givesConstBonus, bool hasRange, QString description, QString arg)
{
    this->refID = refID;
    this->name = name;
    this->givesConstBonus = givesConstBonus;
    this->hasRange = hasRange;
    this->description = description;
    this->description = arg.length() > 0 ? description + " (" + arg + ")" : description;
}
