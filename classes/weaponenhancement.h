#ifndef WEAPONENHANCEMENT_H
#define WEAPONENHANCEMENT_H

#include "weaponproperty.h"

#include <QString>



class WeaponEnhancement
{
public:
    WeaponEnhancement();
    WeaponEnhancement(QString refID, QString name, float priceMultiplier, int priceModifier, WeaponProperty *gained);

    QString refID;
    QString name;
    float priceMultiplier;
    int priceModifier;
    WeaponProperty *gained;
};

#endif // WEAPONENHANCEMENT_H
